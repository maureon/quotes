package quotes.quote;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface QuoteRepository extends PagingAndSortingRepository<Quote,Long> {

}
