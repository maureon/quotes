package quotes;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import quotes.quote.Quote;
import quotes.quote.QuoteRepository;

@SpringBootApplication
public class Application {

	@Autowired
	private QuoteRepository quoteRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean DozerBeanMapper dozerBeanMapper(){
		return new DozerBeanMapper();
	}
	
	@Bean
	public CommandLineRunner demo() {
		return (args) -> {
			quoteRepository.save(new Quote(null,"author1", "text1", "context1"));
			quoteRepository.save(new Quote(null,"author2", "text2", "context2"));
			quoteRepository.save(new Quote(null,"author3", "text3", "context3"));
			quoteRepository.save(new Quote(null,"author4", "text4", "context4"));
			quoteRepository.save(new Quote(null,"author5", "text5", "context5"));
			quoteRepository.save(new Quote(null,"author6", "text6", "context6"));
			quoteRepository.save(new Quote(null,"author7", "text7", "context7"));
			quoteRepository.save(new Quote(null,"author8", "text8", "context8"));
			quoteRepository.save(new Quote(null,"author9", "text9", "context9"));
			quoteRepository.save(new Quote(null,"author10", "text10", "context10"));
		};
	}
}
