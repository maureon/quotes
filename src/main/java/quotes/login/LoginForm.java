package quotes.login;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

public class LoginForm {
	
	@NotNull
	@Email
	private String user;
	
	@NotNull
	@Size(min = 2)
	private String password;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String mail) {
		this.password = mail;
	}
	
	
}
