package quotes.login;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
		
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@ModelAttribute @Valid LoginForm form, BindingResult bindingResult){
		if (bindingResult.hasErrors()){
			return "login";
		}
		return "login";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public ModelAndView login(){
		return new ModelAndView("login", "loginForm", new LoginForm());
	}	
	
	@RequestMapping(value = "/logon", method = RequestMethod.GET)
	public String logon(){
		return "logon";
	}	
	
}
