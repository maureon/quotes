package quotes.ui;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Breadcrumb{

	private List<BreadcrumbPathElement>  pathElements;	
	private String activeElement;
	
	public static  class Builder {
		
		private Breadcrumb breadcrumb = new Breadcrumb();
		
		public Builder elem(String text, String link){
			BreadcrumbPathElement breadcrumbPathElement = new BreadcrumbPathElement(text, link);
			if (breadcrumb.getPathElements() == null){ 
				breadcrumb.setPathElements( new ArrayList<BreadcrumbPathElement>());
			}
			breadcrumb.getPathElements().add(breadcrumbPathElement);
			return this;
		}
		
		public Builder active(String text){
			breadcrumb.setActiveElement(text);
			return this;
		}
		
		public Breadcrumb build(){
			return breadcrumb;
		}
		
	}
	
}
