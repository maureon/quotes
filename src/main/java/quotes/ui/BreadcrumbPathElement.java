package quotes.ui;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class BreadcrumbPathElement {

	private String text;
	private String link;	
	
}
