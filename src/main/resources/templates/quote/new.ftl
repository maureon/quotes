<#include "/header.ftl">
<div class="container">
	<#include "/ui/breadcrumb.ftl">
	<div class="row">
		<form action="/quotes/new" method="post" class="form-horizontal"
			role="form">
			<div class="form-group">
				<label for="text" class="col-lg-2 control-label">Texto</label>
				<div class="col-lg-10">
					<@spring.bind "quoteForm.text"/>
					<textarea id="text" class="form-control" rows="5" name="text"
						placeholder="cita memorable, ej.: Más se perdió en cuba y volvieron cantando"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="autor" class="col-lg-2 control-label">Autor</label>
				<div class="col-lg-10">
					<@spring.bind "quoteForm.author"/> <input type="text"
						class="form-control" id="author" name="author"
						placeholder="autor de la cita si se conoce">
				</div>
			</div>
			<div class="form-group">
				<label for="context" class="col-lg-2 control-label">Contexto</label>
				<div class="col-lg-10">
					<@spring.bind "quoteForm.context"/>
					<textarea id="context" class="form-control" rows="5" name="context"
						placeholder="información circunstancial si se conoce, ej.: En referencia a la Guerra Hispano-Estadounidense de 1898 donde España perdió los territorios de Cuba, Puerto Rico, las Filipinas y Guam en lo que supuso el fin definitivo del imperio español en América."></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-offset-2 col-lg-10">
					<button type="submit" class="btn btn-default">Enviar</button>
				</div>
			</div>
		</form>
	</div>
</div>
<#include "/footer.ftl">